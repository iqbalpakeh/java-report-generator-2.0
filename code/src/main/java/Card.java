/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

/**
 * Class representing a card. It contains information about result,
 * failure position and VTF.
 */
public class Card {

    /**
     * Constant used for PASS result
     */
    public static final String RESULT_PASS = "Pass";

    /**
     * Constant used for FAIL result
     */
    public static final String RESULT_FAIL = "Fail";

    /**
     * Constant used for NT result
     */
    public static final String RESULT_NT = "NT";

    /**
     * card VTF number
     */
    private String mVTF;

    /**
     * Cross testing verdict with this
     * card combination and device being tested
     */
    private String mResult;

    /**
     * Original value read from report
     */
    private String mOriginalFailures;

    /**
     * Observations information from lab. This information is used only for NT cases.
     */
    private String mObservations;

    /**
     * Analyser of Cross Test Result read from Failure Column from report file.
     * This object do the checking and conversion to necessary format.
     *
     * Contain information about the non-pass position. This may contain
     * Distance Failure (DF), Transaction Failure (TF), Communication Failure (CF)
     * and also Not Tested (NT).
     */
    private XTAnalyser mXTAnalyser;

    /**
     * The constructor
     *
     * @param vtf number of card
     * @param failureColumnData as written in Failures column in report document
     * @param result of cross testing between this card and tested device
     * @param observations of issue found by lab
     */
    public Card(String vtf, String failureColumnData, String result, String observations) {
        mVTF = vtf;
        mResult = result;
        mOriginalFailures = failureColumnData;
        mObservations = observations;
        mXTAnalyser = new XTAnalyser(vtf, failureColumnData, result);
    }

    /**
     * Get the vtf number of this card
     *
     * @return VTF number
     */
    public String getVTF() {
        return mVTF;
    }

    /**
     * Get cross testing result between this card and the tested device
     *
     * @return VTF number
     */
    public String getResult() {
        return mResult;
    }

    /**
     * Get analysed result object from this card
     *
     * @return analysed value from XTAnalyser
     */
    public XTAnalyser getAnalysedResult() {
        return mXTAnalyser;
    }

    /**
     * Get original report failures value
     *
     * @return value in Failure column
     */
    public String getFailurePosition() {
        return mOriginalFailures;
    }

    /**
     * Get observations of issue 
     * 
     * @return analysis information from lab
     */
    public String getObservations() {
        return mObservations;
    }
}
