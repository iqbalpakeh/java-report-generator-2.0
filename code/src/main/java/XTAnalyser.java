/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

import java.util.ArrayList;

/**
 * Class contains information of Card Cross Testing for each combination of
 * device and card.
 *
 * It focus on translating the data from device cross test sheet to the simple
 * result data ready to be compared. Complex offset calculation is implemented
 * at this class.
 */
public class XTAnalyser {

    /**
     * Tag for debugging purpose
     */
    private static final String TAG = XTAnalyser.class.getSimpleName();

    /**
     * Contain information about the cross testing result. The structure of this data is
     * the following:
     *     0cm   1cm   2cm   3cm   4cm
     *     NSWEC NSWEC NSWEC NSWEC NSWEC
     *     ##### ##### ##### ##### #####
     *     0     5     10    15    20
     *
     *     #:
     *     P = Pass
     *     D = Distance Failure
     *     C = Communication Failure
     *     T = Transaction Failure
     *     - = Not Tested
     *
     *     example of complete ID1 Pass:
     *     PPPPP ----- ----- PPPPP ----P
     */
    private String mCrossTestResult;

    /**
     * Contain information of report collection based on failure type, distance
     * and direction. This data object is used to generate the report.
     */
    private ArrayList<String> mReportCollection;

    /**
     * Card VTF number. No need to give the value to this variable if this class
     * is used only for expected value.
     */
    private String mVTF;

    /**
     * Determine whether the given failures position is in good format or not.
     * If it's not formated correctly, don't accept the value to be procesed
     * as it may crash the system.
     */
    private boolean mFormatAcceptable;

    /**
     * The constructor
     *
     * @param failureColumnData failure column data information
     */
    public XTAnalyser(String failureColumnData) {
        mFormatAcceptable = true;
        mReportCollection = new ArrayList<>();        
        try {
            String[] positions = checkPositionFormat(failureColumnData);
            buildCrossTestResult(positions);
            buildCrossTestReport(positions);
        } catch (FormatErrorException exception) {
            Logger.log(TAG, "Format error: throw error to avoid crash");
        }
    }

    /**
     * The constructor
     *
     * @param vtf value of card
     * @param failureColumnData failure column data information
     */
    public XTAnalyser(String vtf, String failureColumnData, String result) {
        mVTF = vtf;
        mFormatAcceptable = true;
        mReportCollection = new ArrayList<>();        
        try {
            String[] positions = checkPositionFormat(failureColumnData);
            buildCrossTestResult(positions);
            buildCrossTestReport(positions);
            checkResultFormat(result);
        } catch (FormatErrorException exception) {
            Logger.log(TAG, "FORMAT ERROR @" + vtf + ": throw error to avoid crash");
        }
    }

    /**
     * Check that the acceptable input is only 
     * Pass, Fail and NT.
     * 
     * @param input of card result value
     * @throws FormatErrorException exception to avoid crash
     * @return correctly formatted string, if possible
     */
    public String checkResultFormat(String input) throws FormatErrorException {
        // Check with 3 accetable type only
        if (!input.equals(Card.RESULT_PASS) && 
            !input.equals(Card.RESULT_FAIL) && 
            !input.equals(Card.RESULT_NT)) {
            // Log the error and warn the user
            mFormatAcceptable = false;
            Logger.addError(new Logger.Error(mVTF, 
                    "\"" + input + "\" is not acceptable XT Result format"));
            // throw exeption here
            // to avoid crash!!
            throw new FormatErrorException();
        }
        // retur only corrected format input
        return input;
    }

    /**
     * Check the format of failures column information. It MUST only contains
     * the acceptable value.
     *
     * @param input value of failures column information
     * @throws FormatErrorException exception to avoid crash
     * @return correctly format string, if possible
     */
    private String[] checkPositionFormat(String input) throws FormatErrorException {

        // Character 'x' means the position is not yet accessed. Character 'v' means the position is
        // already accessed. This function should detect if there're two entries
        // pointing the same position multiple times.
        StringBuilder reference =
                new StringBuilder("xxxxx" + "xxxxx" + "xxxxx" + "xxxxx" + "xxxxx");

        // Remove any white space and create an array based on ',' as delimiter
        String[] array = input.replaceAll("\\s+","").split(",");

        // flag and variable to handle duplication error case
        boolean duplicationError = false;
        String duplicationPositions = "";

        // Iterate all entries
        for (int i=0; i<array.length; i++) {
            // Check String length
            if (array[i].length() != 5) {
                // Log the error and warn the user
                mFormatAcceptable = false;
                Logger.addError(new Logger.Error(mVTF,
                        "\"" + array[i] + "\" is not acceptable format"));
                // throw exeption here
                // to avoid crash!!
                throw new FormatErrorException();
            }
            // Check failure type
            if (!array[i].substring(0, 2).equals("CF") &&
                !array[i].substring(0, 2).equals("DF") &&
                !array[i].substring(0, 2).equals("TF") &&
                !array[i].substring(0, 2).equals("NT")) {
                // Log the error and warn the user
                mFormatAcceptable = false;
                Logger.addError(new Logger.Error(mVTF,
                        "\"" + array[i].substring(0, 2) + "\" is not acceptable failure type format"));
                // throw exeption here
                // to avoid crash!!
                throw new FormatErrorException();
            }
            // Check @
            if (array[i].charAt(2) != '@') {
                // Log the error and warn the user
                mFormatAcceptable = false;
                Logger.addError(new Logger.Error(mVTF,
                        "\"" +  array[i].charAt(2) + "\" is not acceptable format"));
                // throw exeption here
                // to avoid crash!!
                throw new FormatErrorException();
            }
            // Check Distance
            if (array[i].charAt(3) != '0' &&
                array[i].charAt(3) != '1' &&
                array[i].charAt(3) != '2' &&
                array[i].charAt(3) != '3' &&
                array[i].charAt(3) != '4') {
                // Log the error and warn the user
                mFormatAcceptable = false;
                Logger.addError(new Logger.Error(mVTF,
                        "\"" + array[i].charAt(3) + "\" is not acceptable format"));
                // throw exeption here
                // to avoid crash!!
                throw new FormatErrorException();
            }
            // Check position
            if (array[i].charAt(4) != 'N' &&
                array[i].charAt(4) != 'S' &&
                array[i].charAt(4) != 'E' &&
                array[i].charAt(4) != 'W' &&
                array[i].charAt(4) != 'C' &&
                array[i].charAt(4) != 'A') {
                // Log the error and warn the user
                mFormatAcceptable = false;
                Logger.addError(new Logger.Error(mVTF,
                        "\"" + array[i].charAt(4) + "\" is not acceptable format"));
                // throw exeption here
                // to avoid crash!!
                throw new FormatErrorException();
            }
            // Check multiple entry
            int offset = calculateOffset(array[i]);
            if (array[i].charAt(4) != 'A') {
                // Hanlde non 'A'
                if (reference.charAt(offset) == 'x') {
                    // first encountered, than change the reference bit
                    reference.setCharAt(offset, 'v');
                } else { // char = 'v'
                    // next encountered, than show error
                    duplicationError = true;
                    duplicationPositions += array[i] + " ";
                }
            } else {
                // hanlde 'A'
                if (reference.substring(offset, offset + 5).equals("xxxxx")) {
                    // first encountered, than change the reference bit
                    reference.replace(offset, offset + 5, "vvvvv");
                } else {
                    // next encountered, than show error
                    duplicationError = true;
                    duplicationPositions += array[i] + " ";
                }
            }
        }

        if (duplicationError == true) {
            mFormatAcceptable = false;
            Logger.addError(new Logger.Error(mVTF, "\"" + 
                    duplicationPositions.trim() + "\" position is already accessed previously"));
        }

        // return only correctly formatted array
        return array;
    }

    /**
     * Check the format of failures column information.
     *
     * @return true if correctly formatted. Otherwise, false.
     */
    public boolean isFormatAcceptable() {
        return mFormatAcceptable;
    }

    /**
     * Build XT data structure. This data is used for checking purpose. 
     * Non-reachable position is also considered while building the cross test result.
     *
     * @param positions of failures column data information
     * @throws FormatErrorException exception to avoid crash 
     */
    private void buildCrossTestResult(String[] positions) throws FormatErrorException {
        // Init result value. All position are pass.
        StringBuilder result = new StringBuilder("PPPPP" + "PPPPP" + "PPPPP" + "PPPPP" + "PPPPP");
        // Update result based on each position
        for (int i=0; i<positions.length; i++) {
            if (positions[i].charAt(4) == 'A') {
                // Handle A
                char type = extractCharacter(positions[i]);
                result.replace(
                    calculateOffset(positions[i]),
                    calculateOffset(positions[i]) + 5,
                    "" + type + type + type + type + type);
            } else {
                // Handle N, S, W, E, C
                result.setCharAt(
                    calculateOffset(positions[i]),
                    extractCharacter(positions[i])
                );
            }
        }
        // Get masking value of non-reachable position
        String[] masking = Configuration.getInstance().getValues(Configuration.LIST_NON_REACHABLE_POSITION);        
        if (!masking[0].equals("") && (mVTF != null)) {            
            for (int i=0; i<masking.length; i++) {
                // Check Distance
                if (masking[i].charAt(0) != '0' &&
                    masking[i].charAt(0) != '1' &&
                    masking[i].charAt(0) != '2' &&
                    masking[i].charAt(0) != '3' &&
                    masking[i].charAt(0) != '4') {
                    // Stop system and warn the user
                    Logger.warning("LIST_NON_REACHABLE_POSITION ERROR : " + masking[i].charAt(0) + " is not acceptable DISTANCE (Only use 0, 1, 2, 3 or 4)");
                    System.exit(0);
                }
                // Check position
                if (masking[i].charAt(1) != 'N' &&
                    masking[i].charAt(1) != 'S' &&
                    masking[i].charAt(1) != 'E' &&
                    masking[i].charAt(1) != 'W' &&
                    masking[i].charAt(1) != 'C' &&
                    masking[i].charAt(1) != 'A') {
                    // Stop system and warn the user
                    Logger.warning("LIST_NON_REACHABLE_POSITION ERROR : " + masking[i].charAt(1) + " is not acceptable POSITION (Only use N, S, E, W, C, or A)");            
                    System.exit(0);        
                }                             
                // Replace non-reachable position marked as NT to Pass
                if (masking[i].charAt(1) == 'A') {
                    // Check masking position has to be marked as NT
                    int offset = calculateOffsetMasking(masking[i]);
                    if (!result.substring(offset, offset+5).equals("-----")) {
                        // Log the error and warn the user
                        mFormatAcceptable = false;
                        Logger.addError(new Logger.Error(mVTF, "\"" + masking[i] + "\" position is not marked as NT"));
                    }
                    // to overwrite '-----' with 'PPPPP'
                    result.replace(offset, offset+5, "PPPPP");
                } else {
                    // Check masking position has to be marked as NT
                    int offset = calculateOffsetMasking(masking[i]);
                    if (result.charAt(offset) != '-') {
                        // Log the error and warn the user
                        mFormatAcceptable = false;
                        Logger.addError(new Logger.Error(mVTF, "\"" + masking[i] + "\" position is not marked as NT"));
                    }
                    // to overwrite '-' with 'P'
                    result.setCharAt(calculateOffsetMasking(masking[i]), 'P');
                }
            }
        }
        // throw exeption here to avoid crash!!
        if (mFormatAcceptable == false) throw new FormatErrorException();
        // Store final result
        mCrossTestResult = result.toString();
    }

    /**
     * Build XT report collection. This data is used for report
     * generating purpose.
     *
     * @param positions of failures column data information
     */
    private void buildCrossTestReport(String[] positions) {

        // Init all failure type local
        // collection objects
        ArrayList<String> DF_0 = new ArrayList<>();
        ArrayList<String> DF_1 = new ArrayList<>();
        ArrayList<String> DF_2 = new ArrayList<>();
        ArrayList<String> DF_3 = new ArrayList<>();
        ArrayList<String> DF_4 = new ArrayList<>();
        ArrayList<String> CF_0 = new ArrayList<>();
        ArrayList<String> CF_1 = new ArrayList<>();
        ArrayList<String> CF_2 = new ArrayList<>();
        ArrayList<String> CF_3 = new ArrayList<>();
        ArrayList<String> CF_4 = new ArrayList<>();
        ArrayList<String> TF_0 = new ArrayList<>();
        ArrayList<String> TF_1 = new ArrayList<>();
        ArrayList<String> TF_2 = new ArrayList<>();
        ArrayList<String> TF_3 = new ArrayList<>();
        ArrayList<String> TF_4 = new ArrayList<>();

        // Collect every error based on
        // failure type and direction
        for (int i=0; i<positions.length; i++) {
            if (extractCharacter(positions[i]) == 'D') {
                if (positions[i].charAt(3) == '0') {
                    if (positions[i].charAt(4) == 'N') {
                        DF_0.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        DF_0.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        DF_0.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        DF_0.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        DF_0.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        DF_0.add("North, South, West, East and Center");
                    }
                } else if (positions[i].charAt(3) == '1') {
                    if (positions[i].charAt(4) == 'N') {
                        DF_1.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        DF_1.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        DF_1.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        DF_1.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        DF_1.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        DF_1.add("All Positions");
                    }
                } else if (positions[i].charAt(3) == '2') {
                    if (positions[i].charAt(4) == 'N') {
                        DF_2.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        DF_2.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        DF_2.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        DF_2.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        DF_2.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        DF_2.add("All Positions");
                    }
                } else if (positions[i].charAt(3) == '3') {
                    if (positions[i].charAt(4) == 'N') {
                        DF_3.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        DF_3.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        DF_3.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        DF_3.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        DF_3.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        DF_3.add("All Positions");
                    }
                } else { // '4'
                    if (positions[i].charAt(4) == 'N') {
                        DF_4.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        DF_4.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        DF_4.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        DF_4.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        DF_4.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        DF_4.add("All Positions");
                    }
                }
            } else if (extractCharacter(positions[i]) == 'C') {
                if (positions[i].charAt(3) == '0') {
                    if (positions[i].charAt(4) == 'N') {
                        CF_0.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        CF_0.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        CF_0.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        CF_0.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        CF_0.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        CF_0.add("All Positions");
                    }
                } else if (positions[i].charAt(3) == '1') {
                    if (positions[i].charAt(4) == 'N') {
                        CF_1.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        CF_1.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        CF_1.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        CF_1.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        CF_1.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        CF_1.add("All Positions");
                    }
                } else if (positions[i].charAt(3) == '2') {
                    if (positions[i].charAt(4) == 'N') {
                        CF_2.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        CF_2.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        CF_2.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        CF_2.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        CF_2.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        CF_2.add("All Positions");
                    }
                } else if (positions[i].charAt(3) == '3') {
                    if (positions[i].charAt(4) == 'N') {
                        CF_3.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        CF_3.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        CF_3.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        CF_3.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        CF_3.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        CF_3.add("All Positions");
                    }
                } else {
                    if (positions[i].charAt(4) == 'N') {
                        CF_4.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        CF_4.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        CF_4.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        CF_4.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        CF_4.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        CF_4.add("All Positions");
                    }
                }
            } else if (extractCharacter(positions[i]) == 'T') {
                if (positions[i].charAt(3) == '0') {
                    if (positions[i].charAt(4) == 'N') {
                        TF_0.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        TF_0.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        TF_0.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        TF_0.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        TF_0.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        TF_0.add("All Positions");
                    }
                } else if (positions[i].charAt(3) == '1') {
                    if (positions[i].charAt(4) == 'N') {
                        TF_1.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        TF_1.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        TF_1.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        TF_1.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        TF_1.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        TF_1.add("All Positions");
                    }
                } else if (positions[i].charAt(3) == '2') {
                    if (positions[i].charAt(4) == 'N') {
                        TF_2.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        TF_2.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        TF_2.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        TF_2.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        TF_2.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        TF_2.add("All Positions");
                    }
                } else if (positions[i].charAt(3) == '3') {
                    if (positions[i].charAt(4) == 'N') {
                        TF_3.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        TF_3.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        TF_3.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        TF_3.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        TF_3.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        TF_3.add("All Positions");
                    }
                } else {
                    if (positions[i].charAt(4) == 'N') {
                        TF_4.add("North");
                    } else if (positions[i].charAt(4) == 'S') {
                        TF_4.add("South");
                    } else if (positions[i].charAt(4) == 'W') {
                        TF_4.add("West");
                    } else if (positions[i].charAt(4) == 'E') {
                        TF_4.add("East");
                    } else if (positions[i].charAt(4) == 'C') {
                        TF_4.add("Center");
                    } else { //positions[i].charAt(4) == 'A'
                        TF_4.add("All Positions");
                    }
                }
            } else { // NT
                // do nothing
            }
        }

        // Iterate all local collection and
        // store to report collection
        prepareSentence(DF_0, "Distance Failures at 0cm");
        prepareSentence(DF_1, "Distance Failures at 1cm");
        prepareSentence(DF_2, "Distance Failures at 2cm");
        prepareSentence(DF_3, "Distance Failures at 3cm");
        prepareSentence(DF_4, "Distance Failures at 4cm");
        prepareSentence(CF_0, "Communication Failures at 0cm");
        prepareSentence(CF_1, "Communication Failures at 1cm");
        prepareSentence(CF_2, "Communication Failures at 2cm");
        prepareSentence(CF_3, "Communication Failures at 3cm");
        prepareSentence(CF_4, "Communication Failures at 4cm");
        prepareSentence(TF_0, "Transaction Failures at 0cm");
        prepareSentence(TF_1, "Transaction Failures at 1cm");
        prepareSentence(TF_2, "Transaction Failures at 2cm");
        prepareSentence(TF_3, "Transaction Failures at 3cm");
        prepareSentence(TF_4, "Transaction Failures at 4cm");
    }

    /**
     * Prepare the sentence for the final report.
     *
     * @param collection of each error position
     * @param header of sentence
     */
    private void prepareSentence(ArrayList<String> collection, String header) {

        String sentence = "";
        int iterator = 0;
        int lastPosition = collection.size() - 1;

        if (collection.size() != 0) {
            for (String data : collection) {
                if (iterator == 0) {
                    sentence = sentence + " ";
                } else if (iterator != lastPosition) {
                    sentence = sentence + ", ";
                } else {
                    sentence = sentence + " and ";
                }
                sentence = sentence + data;
                iterator++;
            }
            // Check if all position are exist
            if (sentence.contains("North") && sentence.contains("South") &&
                sentence.contains("West") && sentence.contains("East") && sentence.contains("Center")) {
                // Change to all positions
                sentence = " All Positions";
            }
            mReportCollection.add(header + sentence);
        }
    }

    /**
     * Calculate the offset of character that need to be changed in the
     * String variable.
     *
     * @param position information from Failures Column
     * @return offset of character
     */
    private int calculateOffset(String position) {
        // Init offset value
        int offset = 0;
        // Check distance:
        char distance = position.charAt(3);
        if (distance == '0') {
            offset = offset + 0;
        } else if (distance == '1') {
            offset = offset + 5;
        } else if (distance == '2') {
            offset = offset + 10;
        } else if (distance == '3') {
            offset = offset + 15;
        } else { // distance == 4
            offset = offset + 20;
        }
        // Check direction:
        char direction = position.charAt(4);
        if (direction == 'N' || direction == 'A') {
            offset = offset + 0;
        } else if (direction == 'S') {
            offset = offset + 1;
        } else if (direction == 'W') {
            offset = offset + 2;
        } else if (direction == 'E') {
            offset = offset + 3;
        } else { // direction == C
            offset = offset + 4;
        }
        // Get final result
        return offset;
    }

    /**
     * Calculate the offset of character from masking that need to be changed in the
     * String variable.
     *
     * @param position information from masking position
     * @return offset of character
     */
    private int calculateOffsetMasking(String position) {
        // Init offset value
        int offset = 0;
        // Check distance:
        char distance = position.charAt(0);
        if (distance == '0') {
            offset = offset + 0;
        } else if (distance == '1') {
            offset = offset + 5;
        } else if (distance == '2') {
            offset = offset + 10;
        } else if (distance == '3') {
            offset = offset + 15;
        } else { // distance == 4
            offset = offset + 20;
        }
        // Check direction:
        char direction = position.charAt(1);
        if (direction == 'N' || direction == 'A') {
            offset = offset + 0;
        } else if (direction == 'S') {
            offset = offset + 1;
        } else if (direction == 'W') {
            offset = offset + 2;
        } else if (direction == 'E') {
            offset = offset + 3;
        } else { // direction == C
            offset = offset + 4;
        }
        // Get final result
        return offset;
    }

    /**
     * Extract the character to be put on the mCrossTestResult object
     *
     * @param position information from Failures Column
     * @return character to be used by mCrossTestResult
     */
    private char extractCharacter(String position) {
        // Init character
        char extractChar;
        // Get failure type
        String failureType = position.substring(0, 2);
        if (failureType.equals("CF")) {
            extractChar = 'C';
        } else if(failureType.equals("DF")) {
            extractChar = 'D';
        } else if(failureType.equals("TF")) {
            extractChar = 'T';
        } else { // NT
            extractChar = '-';
        }
        // Get final character
        return extractChar;
    }

    /**
     * Get value converted value
     *
     * @return converted value
     */
    public String getCrossTestResult() {
        return mCrossTestResult;
    }

    /**
     * check if xt result contains any error
     *
     * @return true if there's failures. Otherwise, return false.
     */
    public boolean checkContainError() {
        return (mCrossTestResult.contains("C") ||
                mCrossTestResult.contains("D") || mCrossTestResult.contains("T"));
    }

    /**
     * check if there's a failure at 3 cm
     *
     * @return true if there's failures. Otherwise, return false.
     */
    public boolean check3cmFailed() {

        boolean check = false;
        String P3 = mCrossTestResult.substring(15, 20);
        // Check North
        if (P3.charAt(0) != 'P') {
            check = true;
        }
        // Check South
        if (P3.charAt(1) != 'P') {
            check = true;
        }
        // Check West
        if (P3.charAt(2) != 'P') {
            check = true;
        }
        // Check East
        if (P3.charAt(3) != 'P') {
            check = true;
        }
        // Check Center
        if (P3.charAt(4) != 'P') {
            check = true;
        }

        return check;

    }

    /**
     * check if 2 cm is retested
     *
     * @return true if there's failures. Otherwise, return false.
     */
    public boolean check2cmRetested() {

        boolean check = true;
        String P2 = mCrossTestResult.substring(10, 15);
        // Check North
        if (P2.charAt(0) == '-') {
            check = false;
        }
        // Check South
        if (P2.charAt(1) == '-') {
            check = false;
        }
        // Check West
        if (P2.charAt(2) == '-') {
            check = false;
        }
        // Check East
        if (P2.charAt(3) == '-') {
            check = false;
        }
        // Check Center
        if (P2.charAt(4) == '-') {
            check = false;
        }

        return check;

    }

    /**
     * Get collection of report
     *
     * @return report collection
     */
    public ArrayList<String> getCrossTestReport() {
        return mReportCollection;
    }

}
