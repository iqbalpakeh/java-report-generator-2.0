/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

public class App {

    /**
     * Program main function
     *
     * @param args[] arguments input
     */
    public static void main(String[] args) {
        // Show program name and version number
        Logger.warning("\nReport Auto Checking Program, Version " + Configuration.VERSION + "\n");

        // Activate Logger
        if (args.length != 0) {
            if (args[0].equals("-verbose")) {
                Logger.setVerboseMode(true);
            } else {
                Logger.setVerboseMode(false);
                Logger.warning("Sample cmd -> $ java -jar report.jar -verbose");
                System.exit(1);
            }
        }

        // Show cross test result data structure to user
        Logger.warning("Cross Testing data structure:");
        Logger.warning("    0cm   1cm   2cm   3cm   4cm");
        Logger.warning("    NSWEC NSWEC NSWEC NSWEC NSWEC");
        Logger.warning("    ##### ##### ##### ##### #####");
        Logger.warning("    0     5     10    15    20");
        Logger.warning("");
        Logger.warning("    #:");
        Logger.warning("    P = Pass");
        Logger.warning("    D = Distance Failure");
        Logger.warning("    C = Communication Failure");
        Logger.warning("    T = Transaction Failure");
        Logger.warning("    - = Not Tested");
        Logger.warning("");
        Logger.warning("    example of complete ID1 Pass:");
        Logger.warning("    PPPPP ----- ----- PPPPP ----P");
        Logger.warning("");

        // Execute parser class
        Parser parser = new Parser();
        parser.read();
        parser.check();
        parser.generate();
    }

}
