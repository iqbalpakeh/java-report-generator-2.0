/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

/**
 * Class responsible to help Parser class to show more details information to 
 * the user. 
 */
public class ParserHelper {

    /**
     * Tag for debugging purpose
     */
    private static final String TAG = ParserHelper.class.getSimpleName();

    /**
     * Check if vtf exist in the vtfs list.
     *
     * @param list of cards vtf
     * @param vtf of checked card
     * @return true if found. Otherwise, false
     */
    public static boolean checkArrayContain(String[] list, String vtf) {
        for (int i=0; i<list.length; i++) {
            if (list[i].equals(vtf)) {
                //VTF is found
                return true;
            }
        }
        // VTF is not found
        return false;
    }

    /**
     * Generate XT position name from array position
     * 
     * @param pos of XT
     * @return name of position
     */
    public static String generatePosition(int pos) {
        
        String location = "";
        
        switch(pos) {
            case 0: location = "0N"; 
                    break;
            case 1: location = "0S"; 
                    break;
            case 2: location = "0W"; 
                    break;
            case 3: location = "0E"; 
                    break;                                        
            case 4: location = "0C"; 
                    break;
            case 5: location = "1N"; 
                    break;
            case 6: location = "1S"; 
                    break;
            case 7: location = "1W"; 
                    break;
            case 8: location = "1E"; 
                    break;                                        
            case 9: location = "1C"; 
                    break;     
            case 10:location = "2N"; 
                    break;
            case 11:location = "2S"; 
                    break;
            case 12:location = "2W"; 
                    break;
            case 13:location = "2E"; 
                    break;
            case 14:location = "2C"; 
                    break; 
            case 15:location = "3N"; 
                    break;
            case 16:location = "3S"; 
                    break;
            case 17:location = "3W"; 
                    break;
            case 18:location = "3E"; 
                    break;
            case 19:location = "3C"; 
                    break; 
            case 20:location = "4N"; 
                    break;
            case 21:location = "4S"; 
                    break;
            case 22:location = "4W"; 
                    break;
            case 23:location = "4E"; 
                    break;
            case 24:location = "4C"; 
                    break;                                                                                               
        }
        return location;
    }    

    /**
     * Build expected string to be read by user. If position need to be tested,
     * expected value is '*'.
     *
     * @param expectedResult template of expected results
     * @param actual result of failures
     * @return string of expected values
     */
    public static String buildExpectedResult(String expectedResult, String actualResult) {
        
        char[] actual = actualResult.toCharArray();
        char[] ref = expectedResult.toCharArray();
        char[] res = new char[25];
        
        for (int i=0; i<ref.length; i++) {
            if ((ref[i] == '*') && (actual[i] != '-')) {
                res[i] = actual[i];
            } else if ((ref[i] == '*') && (actual[i] == '-')) {
                res[i] = ref[i];
            } else {
                res[i] = '-';
            }
        }
        return new String(res);
    }

    /**
     * Find positions that should be NT but not marked as NT
     * 
     * @param expectedNT of the XT result
     * @param actualResponse of the XT result
     * @return message showing error position
     */
    public static String findErrorNTPositions(String expectedNT, String actualResponse) {
        
        String errorMessage = "Positions ";
            
        char[] ref = expectedNT.toCharArray();
        char[] actual = actualResponse.toCharArray();
        
        for (int i=0; i<ref.length; i++) {
            if ((ref[i] == '-') && (actual[i] != '-')) {
                errorMessage += generatePosition(i) + " ";
            } 
        }
        
        errorMessage += "should be marked as NT.";
        
        return errorMessage;
    }    

    /**
     * Find positions that should be CF but not marked as CF
     * 
     * @param expectedCF of the XT result
     * @param actualResponse of the XT result
     * @return message showing error position
     */
    public static String findErrorCFPositions(String expectedCF, String actualResponse) {
        
        String errorCFMessage = "";
        String errorNTMessage = "";
                
        char[] ref = expectedCF.toCharArray();
        char[] actual = actualResponse.toCharArray();
            
        for (int i=0; i<ref.length; i++) {
            if ((ref[i] == 'C') && (actual[i] != 'C')) {
                errorCFMessage += generatePosition(i) + " ";
            } 
            if ((ref[i] == '-') && (actual[i] != '-')) {
                errorNTMessage += generatePosition(i) + " ";
            }
        }
            
        if (!errorCFMessage.equals("")) {
            errorCFMessage = "Positions " + errorCFMessage;
            errorCFMessage += "should be marked as CF. ";
        }

        if (!errorNTMessage.equals("")) {
            errorNTMessage = "Positions " + errorNTMessage;
            errorNTMessage += "should be marked as NT.";
        }
        
        return errorCFMessage + errorNTMessage;
    }
        
    /**
     * Find positions that should be TF but not marked as TF
     * 
     * @param expectedTF of the XT result
     * @param actualResponse of the XT result
     * @return message showing error position
     */
    public static String findErrorTFPositions(String expectedTF, String actualResponse) {
                
        String errorTFMessage = "";
        String errorNTMessage = "";
                        
        char[] ref = expectedTF.toCharArray();
        char[] actual = actualResponse.toCharArray();
                    
        for (int i=0; i<ref.length; i++) {
            if ((ref[i] == 'T') && (actual[i] != 'T')) {
                errorTFMessage += generatePosition(i) + " ";
            } 
            if ((ref[i] == '-') && (actual[i] != '-')) {
                errorNTMessage += generatePosition(i) + " ";
            }
        }
                    
        if (!errorTFMessage.equals("")) {
            errorTFMessage = "Positions " + errorTFMessage;
            errorTFMessage += "should be marked as TF. ";
        }

        if (!errorNTMessage.equals("")) {
            errorNTMessage = "Positions " + errorNTMessage;
            errorNTMessage += "should be marked as NT.";
        }
                    
        return errorTFMessage + errorNTMessage;
    }  
            
    /**
     * Find positions that should be PASS but not marked as PASS
     * 
     * @param expectedPass of the XT result
     * @param actualResponse of the XT result
     * @return message showing error position
     */
    public static String findErrorPassPositions(String expectedPass, String actualResponse) {
        
        String errorMessage = "Positions ";
                
        char[] ref = expectedPass.toCharArray();
        char[] actual = actualResponse.toCharArray();
                    
        for (int i=0; i<ref.length; i++) {
            if ((ref[i] == 'P') && (actual[i] != 'P')) {
                errorMessage += generatePosition(i) + " ";
            } 
        }
                    
        errorMessage += "should be marked as PASS.";
                    
        return errorMessage; 
    }
    
}