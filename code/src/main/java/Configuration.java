/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class responsible to read and check configuration file
 * and also provide to the user class
 */
public class Configuration {

    /**
     * Version of the tools. Please update this version everytime new
     * version is about to released.
     */
    public static final String VERSION = "2.0";

    /**
     * The constant used to access DEVICE_CONFIGURATION property.
     */
    public static final String DEVICE_CONFIGURATION = "DEVICE_CONFIGURATION";

    /**
     * The constant used to access INPUT_FILE_NAME property.
     */
    public static final String INPUT_FILE_NAME = "INPUT_FILE_NAME";

    /**
     * The constant used to access OUTPUT_FILE_NAME property.
     */
    public static final String OUTPUT_FILE_NAME = "OUTPUT_FILE_NAME";

    /**
     * The constant used to access LIST_NON_ID1 property.
     */
    public static final String LIST_NON_ID1 = "LIST_NON_ID1";

    /**
     * The constant used to access LIST_MSD_ONLY property.
     */
    public static final String LIST_MSD_ONLY = "LIST_MSD_ONLY";

    /**
     * The constant used to access LIST_EXPECTED_NT property.
     */
    public static final String LIST_EXPECTED_NT = "LIST_EXPECTED_NT";

    /**
     * The constant used to access LIST_NON_REACHABLE_POSITION property.
     */    
    public static final String LIST_NON_REACHABLE_POSITION = "LIST_NON_REACHABLE_POSITION";

    /**
     * The constant used for MSD only device.
     */
    public static final String TYPE_MSD = "MSD";

    /**
     * The constant used for qVSDC only device.
     */
    public static final String TYPE_QVSDC = "qVSDC";

    /**
     * The constant used for MSD and qVSDC device.
     */
    public static final String TYPE_QVSDC_AND_MSD = "qVSDC_AND_MSD";

    /**
     * Configuration file location
     */
    public static final String CONFIG_PATH = "config";

    /**
     * Configuration file name
     */
    public static final String CONFIG_FILE = "config.properties";

    /**
     * Field contains information of properties
     * defined in config.properties
     */
    private Properties mProperties;

    /**
     * Input stream to read config.properties
     */
    private InputStream mInput;

    /**
     * Static object for singletone implementation
     */
    private static Configuration mConfiguration;

    /**
     * The constructor
     */
    private Configuration() {
        // Read configuration file
        try {
            mProperties = new Properties();
            mInput = new FileInputStream(CONFIG_PATH + File.separator + CONFIG_FILE);
            mProperties.load(mInput);
            mInput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create singleton configuration object
     */
    public static Configuration getInstance() {
        if (mConfiguration == null) {
            mConfiguration = new Configuration();
            mConfiguration.checkConfiguration();
            Logger.debug("@getInstance", "Create configuration object");
        }
        return mConfiguration;
    }

    /**
     * Function to check all the value of the configuration file
     */
    private void checkConfiguration() {

        String value = "";

        //Check INPUT_FILE_NAME:
        value = mProperties.getProperty(INPUT_FILE_NAME);
        if (!value.substring(value.length()-4, value.length()).equals(".xls")) {
            Logger.warning("\nERROR: Input file name: " + value);
            Logger.warning("ERROR: INPUT_FILE_NAME must have extension [*.xls]. Please fix it.");
            System.exit(0);
        } else {
            Logger.warning("\nInput file name: \n" + value);
        }

        //Check OUTPUT_FILE_NAME:
        value = mProperties.getProperty(OUTPUT_FILE_NAME);
        if (!value.substring(value.length()-4, value.length()).equals(".xls")) {
            Logger.warning("\nERROR: Output file name: " + value);
            Logger.warning("ERROR: OUTPUT_FILE_NAME must have extension [*.xls]. Please fix it.");
            System.exit(0);
        } else {
            Logger.warning("\nOutput file name: \n" + value);
        }

        //Check DEVICE_CONFIGURATION:
        value = mProperties.getProperty(DEVICE_CONFIGURATION);
        if (!value.equals(TYPE_QVSDC) && !value.equals(TYPE_MSD) && !value.equals(TYPE_QVSDC_AND_MSD)) {
            Logger.warning("\nERROR: Device configuration: " + value);
            Logger.warning("ERROR: DEVICE_CONFIGURATION is must be one of [qVSDC], [MSD], [qVSDC_AND_MSD]. Please fix it");
            System.exit(0);
        } else {
            Logger.warning("\nDevice configuration: \n" + value);
        }

        // Check LIST_NON_ID1:
        Logger.warning("\nList of NON-ID1 cards: \n" + mProperties.getProperty(LIST_NON_ID1));

        // Check LIST_MSD_ONLY:
        Logger.warning("\nList of MSD-Only cards: \n" + mProperties.getProperty(LIST_MSD_ONLY));

        // Check LIST_EXPECTED_NT:
        Logger.warning("\nList of expected NT cards: \n" + mProperties.getProperty(LIST_EXPECTED_NT));

        // Check LIST_NON_REACHABLE_POSITION:
        Logger.warning("\nList of non-reachable positions: \n" + mProperties.getProperty(LIST_NON_REACHABLE_POSITION) + "\n");
    }

    /**
     * Function to get single key value
     *
     * @param key name of key
     * @return value of single key
     */
    public String getValue(String key) {
        return mProperties.getProperty(key);
    }

    /**
     * Function to get multiple key values (list type)
     *
     * @param key name of key
     * @return list of values
     */
    public String[] getValues(String key) {
        return mProperties.getProperty(key).split(",");
    }

}
