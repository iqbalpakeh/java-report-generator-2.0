/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.VerticalAlignment;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 * Class responsible to read and write the excel file.
 */
public class Parser {

    /**
     * Tag for debugging purpose
     */
    private static final String TAG = Parser.class.getSimpleName();

    /**
     * VTF column position on lab report document
     */
    private static final int COLUMN_VTF = 1;

    /**
     * Result column position on lab report document
     */
    private static final int COLUMN_RESULT = 3;

    /**
     * Observations column position on lab report document
     */
    private static final int COLUMN_OBSERVATIONS = 5;

    /**
     * Failures column position on lab report document
     */
    private static final int COLUMN_FAILURES = 6;

    /**
     * List of card contain cross test result information for each combination
     */
    private ArrayList<Card> mCardPool;

    /**
     * List of pass card. This list will be used one time during
     * report generation process.
     */
    private ArrayList<Card> mPassCardPool;

    /**
     * List of failed card. This list will be used one time during
     * report generation process.
     */
    private ArrayList<Card> mFailedCardPool;

    /**
     * List of NTs card. This list will be used one time during
     * report generation process.
     */
    private ArrayList<Card> mNTCardPool;

    /**
     * List of Non ID1 cards
     */
    private String[] mNonID1List; 

    /**
     * List of MSD Only card
     */
    private String[] mMsdOnlyList;        

    /**
     * List of Expected NTs cards
     */
    private String[] mExpectedNTList;

    /**
     * The constructor
     */
    public Parser() {
        // Init list objects
        mCardPool = new ArrayList<>();
        mPassCardPool = new ArrayList<>();
        mFailedCardPool = new ArrayList<>();
        mNTCardPool = new ArrayList<>();  
    }

    /**
     * Read the content of excell file
     */
    public void read() {

        try {
            
            // Read excel sheet
            File file = new File(Configuration.CONFIG_PATH + File.separator + Configuration.getInstance().getValue(Configuration.INPUT_FILE_NAME));
            Workbook workbook;
            workbook = Workbook.getWorkbook(file);
            Sheet sheet = workbook.getSheet("Cross_test_results");
            int row = 3;

            // Parse each of card list
            while(!sheet.getCell(COLUMN_VTF, row).getContents().equals("")) {

                String vtf = sheet.getCell(COLUMN_VTF, row).getContents();
                String failures = sheet.getCell(COLUMN_FAILURES, row).getContents();
                String result = sheet.getCell(COLUMN_RESULT, row).getContents();
                String observations = sheet.getCell(COLUMN_OBSERVATIONS, row).getContents();

                // Log card details
                Logger.log(TAG, "cell[" + COLUMN_VTF + "," + row + "] = " + "VTF: " + vtf);
                Logger.log(TAG, "cell[" + COLUMN_FAILURES + "," + row + "] = " + "VTF: " + failures);
                Logger.log(TAG, "cell[" + COLUMN_RESULT + "," + row + "] = " + "VTF: " + result);
                Logger.log(TAG, "cell[" + COLUMN_OBSERVATIONS + "," + row + "] = " + "VTF: " + observations);

                // Create card object
                Card card = new Card(vtf, failures, result, observations);

                // Add card object to necessary list
                mCardPool.add(card);
                switch(result) {
                    case(Card.RESULT_PASS): 
                        mPassCardPool.add(card);
                        break;
                    case(Card.RESULT_FAIL): 
                        mFailedCardPool.add(card);
                        break;
                    case(Card.RESULT_NT):   
                        mNTCardPool.add(card);
                        break;
                }

                // continue iteration
                row++;
            }
        } catch (BiffException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Check the consistency of failures position in the context of
     * vtf, verdict and also form factor.
     *
     * If any of this checking find a failures, this application warns user the error.
     * Information of failure contains of VTF, failure position and why it's failed.
     */
    public void check() {

        // Get all type list
        mNonID1List = Configuration.getInstance().getValues(Configuration.LIST_NON_ID1); 
        mMsdOnlyList = Configuration.getInstance().getValues(Configuration.LIST_MSD_ONLY);        
        mExpectedNTList = Configuration.getInstance().getValues(Configuration.LIST_EXPECTED_NT);

        // Check cross test result one by one
        for (Card card : mCardPool) {
            // Check only acceptable formatted result
            if (card.getAnalysedResult().isFormatAcceptable()) {
                // Check each card based on its type
                if (ParserHelper.checkArrayContain(mExpectedNTList, card.getVTF())) {
                    // check expected NT card
                    Logger.log(TAG, card.getVTF() + " is NT Card");
                    checkExpectedNTCard(card);
                } else if (ParserHelper.checkArrayContain(mMsdOnlyList, card.getVTF())) {
                    // check MSD-only card
                    Logger.log(TAG, card.getVTF() + " is MSD-only Card");
                    checkMSDOnlyCard(card);
                } else if (ParserHelper.checkArrayContain(mNonID1List, card.getVTF())) {
                    // check non ID1 card
                    Logger.log(TAG, card.getVTF() + " is NON-ID1 Card");
                    checkNonID1Card(card);
                } else {
                    // check ID1 card
                    Logger.log(TAG, card.getVTF() + " is ID1 Card");
                    checkID1Card(card);
                }
            }
        }
    }

    /**
     * Execute cross test checking result for ID1 card
     *
     * @param card to be checked
     */
    private void checkID1Card(Card card) {

        XTAnalyser expectedResult = new XTAnalyser("NT@1A, NT@2N, NT@2S, NT@2E, NT@2W, NT@4N, NT@4S, NT@4E, NT@4W");
        XTAnalyser actualResult = card.getAnalysedResult();
        String cardResult = card.getResult();

        // Check CF:
        if (actualResult.getCrossTestResult().contains("C")) {
            
            String reference = "CCCCC" + "-----" + "----C" + "CCCCC" + "----C";

            // CF MUST be at all position.
            if (actualResult.getCrossTestResult().equals(reference)) {
                // No error found
                Logger.log(TAG, "No error found");
                return;
            } else {
                // Failure:
                Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "CF MUST be at all position. " + 
                        ParserHelper.findErrorCFPositions(reference, actualResult.getCrossTestResult()),
                        reference,
                        actualResult.getCrossTestResult()));
                // Out of the function once
                // failure is found
                return;                        
            }
        }

        // Check TF:
        if (actualResult.getCrossTestResult().contains("T")) {

            String reference = "TTTTT" + "-----" + "----T" + "TTTTT" + "----T";

            // TF MUST be at all position.
            if (actualResult.getCrossTestResult().equals(reference)) {
                // No error found
                Logger.log(TAG, "No error found");
                return;
            } else {
                // Failure:
                Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "TF MUST be at all position. " +
                        ParserHelper.findErrorTFPositions(reference, actualResult.getCrossTestResult()),
                        reference,
                        actualResult.getCrossTestResult()));
                // Out of the function once
                // failure is found
                return;                        
            }
        }

        // Check when ID1 card have PASS result
        if (cardResult.equals(Card.RESULT_PASS)) {
            // Check Failures Column:
            if (!expectedResult.getCrossTestResult().equals(actualResult.getCrossTestResult())) {
                // Failure:
                Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "One of the value in failure column is not accepted. " + 
                        ParserHelper.findErrorPassPositions(expectedResult.getCrossTestResult(), actualResult.getCrossTestResult()),
                        expectedResult.getCrossTestResult(),
                        actualResult.getCrossTestResult()));
                // Out of the function once
                // failure is found
                return;                        
            } else {
                // No error found
                Logger.log(TAG, "No error found");
            }
        // Check when ID1 card have FAIL result
        } else if (cardResult.equals(Card.RESULT_FAIL)) {
            // Make sure there's a failure written in the report Failures Column
            if (!expectedResult.getCrossTestResult().equals(actualResult.getCrossTestResult())) {

                if (actualResult.checkContainError()) {
                    // If there's failure at 3 cm, check that
                    // is retested at 2 cm
                    if (actualResult.check3cmFailed()) {

                        if (actualResult.check2cmRetested()) {
                            // No error found
                            Logger.log(TAG, "No error found");
                        } else {
                            // Failure:
                            Logger.addError(new Logger.Error(
                                    card.getVTF(),
                                    "2cm (except 2C) positions is not retested when there's a failure at 3cm",
                                    ParserHelper.buildExpectedResult("*****" + "-----" + "*****" + "*****" + "----*", actualResult.getCrossTestResult()),                                    
                                    actualResult.getCrossTestResult()));
                            // Out of the function once
                            // failure is found
                            return;                                    
                        }

                    } else {
                        // No error found
                        Logger.log(TAG, "No error found");
                    }

                } else {
                    // Failure:
                    Logger.addError(new Logger.Error(
                            card.getVTF(),
                            "ID1 card is Failed, but no error is detected. Please find the reason"));
                    // Out of the function once
                    // failure is found
                    return;                            
                }

            } else {
                // Failure:
                Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "ID1 card is Failed, but the Failures column shows all pass. Please find the reason."));
                // Out of the function once
                // failure is found
                return;                        
            }
        // Check when ID1 card have NT result
        } else {
            // Failure:
            Logger.addError(new Logger.Error(
                    card.getVTF(),
                    "Card is not listed as expected NT. Find the reason why card is NT.",
                    Card.RESULT_PASS + " or " + Card.RESULT_FAIL,
                    cardResult));
            // Out of the function once
            // failure is found
            return;                    
        }

        // Check mandatory NT positions (1cm and 4cm (other than Center))
        if (!actualResult.getCrossTestResult().substring(5, 10).equals("-----") ||
        !actualResult.getCrossTestResult().substring(20, 24).equals("----") ||
        (!actualResult.getCrossTestResult().substring(10, 14).equals("----") && !actualResult.check3cmFailed())) {
        
            if (actualResult.check3cmFailed()) {

                String reference = "*****" + "-----" + "*****" + "*****" + "----*";

                // Failure:
                Logger.addError(new Logger.Error(
                    card.getVTF(),
                    "Mandatory NT's positions are not NT. Please check 1cm, 2cm and 4cm. " + 
                    ParserHelper.findErrorNTPositions(reference, actualResult.getCrossTestResult()),
                    ParserHelper.buildExpectedResult(reference, actualResult.getCrossTestResult()),
                    actualResult.getCrossTestResult()));
                // Out of the function once
                // failure is found
                return;                    
            } else {

                String reference = "*****" + "-----" + "----*" + "*****" + "----*";

                // Failure:
                Logger.addError(new Logger.Error(
                    card.getVTF(),
                    "Mandatory NT's positions are not NT. Please check 1cm, 2cm and 4cm. " + 
                    ParserHelper.findErrorNTPositions(reference, actualResult.getCrossTestResult()),
                    ParserHelper.buildExpectedResult(reference, actualResult.getCrossTestResult()),
                    actualResult.getCrossTestResult()));
                // Out of the function once
                // failure is found
                return;                    
            }

        }        

    }

    /**
     * Execute cross test checking result for Non ID1 card. That is for both
     * Sticker and Microtag
     *
     * @param card to be checked
     */
    private void checkNonID1Card(Card card) {

        XTAnalyser expectedResult = new XTAnalyser("NT@3A, NT@4A");
        XTAnalyser actualResult = card.getAnalysedResult();
        String cardResult = card.getResult();

        // Check CF:
        if (actualResult.getCrossTestResult().contains("C")) {

            String reference = "CCCCC" + "CCCCC" + "CCCCC" + "-----" + "-----";

            // CF MUST be at all position.
            if (actualResult.getCrossTestResult().equals(reference)) {
                // No error found
                Logger.log(TAG, "No error found");
                return;
            } else {
                // Failure:
                Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "CF MUST be at all position. " +
                        ParserHelper.findErrorCFPositions(reference, actualResult.getCrossTestResult()),
                        reference,
                        actualResult.getCrossTestResult()));
                // Out of the function once
                // failure is found
                return;                        
            }
        }

        // Check TF:
        if (actualResult.getCrossTestResult().contains("T")) {

            String reference = "TTTTT" + "TTTTT" + "TTTTT" + "-----" + "-----";

            // TF MUST be at all position.
            if (actualResult.getCrossTestResult().equals(reference)) {
                // No error found
                Logger.log(TAG, "No error found");
                return;
            } else {
                // Failure:
                Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "TF MUST be at all position. " + 
                        ParserHelper.findErrorTFPositions(reference, actualResult.getCrossTestResult()),
                        reference,
                        actualResult.getCrossTestResult()));
                // Out of the function once
                // failure is found
                return;                        
            }
        }

        // Check when Non-ID1 card have PASS result
        if (cardResult.equals(Card.RESULT_PASS)) {
            // Check Failures Column:
            if (!expectedResult.getCrossTestResult().equals(actualResult.getCrossTestResult())) {
                // Failure:
                Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "One of the value in failure column is not accepted. " + 
                        ParserHelper.findErrorPassPositions(expectedResult.getCrossTestResult(), actualResult.getCrossTestResult()),
                        expectedResult.getCrossTestResult(),
                        actualResult.getCrossTestResult()));
                // Out of the function once
                // failure is found
                return;                        
            } else {
                // No error found
                Logger.log(TAG, "No error found");
            }
        // Check when Non-ID1 card have FAIL result
        } else if (cardResult.equals(Card.RESULT_FAIL)) {
            // Make sure there's a failure written in the report Failures Column
            if (!expectedResult.getCrossTestResult().equals(actualResult.getCrossTestResult())) {

                if (actualResult.checkContainError()) {
                    // No error found
                    Logger.log(TAG, "No error found");

                } else {
                    // Failure:
                    Logger.addError(new Logger.Error(
                            card.getVTF(),
                            "Non-ID1 card is Failed, but no error is detected. Please find the reason."));
                    // Out of the function once
                    // failure is found
                    return;                            
                }

            } else {
                // Failure:
                Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "Non-ID1 card is Failed, but the Failures column shows it pass. Please find the reason."));
                // Out of the function once
                // failure is found
                return;
            }
        // Check when Non-ID1 card have NT result
        } else {
            // Failure:
            Logger.addError(new Logger.Error(
                    card.getVTF(),
                    "card is not listed as expected NT. Find the reason why card is NT.",
                    Card.RESULT_PASS + " or " + Card.RESULT_FAIL,
                    cardResult));
            // Out of the function once
            // failure is found
            return;                    
        }

        // Check mandatory NT positions (3cm and 4cm)
        if (!actualResult.getCrossTestResult().substring(15, 25).equals("-----" + "-----")) {

            String reference = "*****" + "*****" + "*****" + "-----" + "-----";

            // Failure:
            Logger.addError(new Logger.Error(
                    card.getVTF(),
                    "Mandatory NT's positions are not NT. Please check 3cm and 4cm. " + 
                    ParserHelper.findErrorNTPositions(reference, actualResult.getCrossTestResult()),
                    ParserHelper.buildExpectedResult(reference, actualResult.getCrossTestResult()),
                    actualResult.getCrossTestResult()));
            // Out of the function once
            // failure is found
            return;                    
        }

    }

    /**
     * Execute cross test checking result for expected NT card. This could be ID1
     * or Non ID1 card
     *
     * @param card to be checked
     */
    private void checkExpectedNTCard(Card card) {

        XTAnalyser expectedResult = new XTAnalyser("NT@0A, NT@1A, NT@2A, NT@3A, NT@4A");
        XTAnalyser actualResult = card.getAnalysedResult();
        String cardResult = card.getResult();

        // Check Result Column:
        if (!cardResult.equals(Card.RESULT_NT)) {
            // Failure:
            Logger.addError(new Logger.Error(
                    card.getVTF(),
                    "must have NT result because listed in Expected NT List",
                    Card.RESULT_NT,
                    cardResult));
            // Out of the function once
            // failure is found                    
            return;
        } else {
            // No error found
            Logger.log(TAG, "No error found");
        }

        // Check Failures Column:
        if (!expectedResult.getCrossTestResult().equals(actualResult.getCrossTestResult())) {
            // Failure:
            Logger.addError(new Logger.Error(
                    card.getVTF(),
                    "One of the value in failure column is not accepted: " + card.getFailurePosition(),
                    expectedResult.getCrossTestResult(),
                    actualResult.getCrossTestResult()));
            // Out of the function once
            // failure is found                     
            return;        
        } else {
            // No error found
            Logger.log(TAG, "No error found");
        }
    }

    /**
     * Execute cross test checking result for MSD-Only card.
     *
     * @param card to be checked
     */
    private void checkMSDOnlyCard(Card card) {

        XTAnalyser expectedResult = new XTAnalyser("NT@0A, NT@1A, NT@2A, NT@3A, NT@4A");
        XTAnalyser actualResult = new XTAnalyser(card.getFailurePosition());

        String deviceConfiguration = Configuration.getInstance().getValue(Configuration.DEVICE_CONFIGURATION);
        String cardResult = card.getResult();

        String[] expectedNTList = Configuration.getInstance().getValues(Configuration.LIST_EXPECTED_NT);

        // Check Result Column:
        if (deviceConfiguration.equals(Configuration.TYPE_QVSDC_AND_MSD)) {

            if (ParserHelper.checkArrayContain(expectedNTList, card.getVTF())) {
            
                if (cardResult.equals(Card.RESULT_NT)) {
                    // No error found
                    Logger.log(TAG, "No error found");
                    
                } else {
                    // Failure:
                    Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "Result must be NT as it's is listed as expected NT"));
                    // Out of the function once
                    // failure is found                         
                    return;
                }

            } else {

                if (cardResult.equals(Card.RESULT_NT)) {
                    // Failure:
                    Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "Check why result is NT as it's not listed in expected NT list"));
                    // Out of the function once
                    // failure is found                         
                    return;
                } else {
                    // No error found
                    Logger.log(TAG, "No error found");
                }

            }

        } else if (deviceConfiguration.equals(Configuration.TYPE_QVSDC)) {

            if (ParserHelper.checkArrayContain(expectedNTList, card.getVTF())) {  

                if (cardResult.equals(Card.RESULT_NT)) {
                    // No error found
                    Logger.log(TAG, "No error found");
                } else {
                    // Failure:
                    Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "Result must be NT as it's is listed as expected NT"));
                    // Out of the function once
                    // failure is found                         
                    return;
                }

            } else {

                if (cardResult.equals(Card.RESULT_NT)) {
                    // No error found
                    Logger.log(TAG, "No error found");
                } else {
                    // Failure:
                    Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "qVSDC device must have NT result if tested with MSD-Only card and card is not listed as expected NT"));
                    // Out of the function once
                    // failure is found    
                    return;                                        
                }

            }

        } else { // MSD 

            if (ParserHelper.checkArrayContain(expectedNTList, card.getVTF())) {  
            
                if (cardResult.equals(Card.RESULT_NT)) {
                    // No error found
                    Logger.log(TAG, "No error found");                
                } else {
                    // Failure:
                    Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "Check why card return NT as it's not list in expected NT list")); 
                    // Out of the function once
                    // failure is found    
                    return;
                }

            } else {

                if (cardResult.equals(Card.RESULT_NT)) {
                    // Failure:
                    Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "Check why card return NT as it's not list in expected NT list"));                    
                    // Out of the function once
                    // failure is found                        
                    return; 
                } else {
                    // No error found
                    Logger.log(TAG, "No error found");                          
                }

            }
        }

        // Check Failures Column:
        if (cardResult.equals(Card.RESULT_NT)) {

            if (!expectedResult.getCrossTestResult().equals(actualResult.getCrossTestResult())) {
                // Failure:
                Logger.addError(new Logger.Error(
                        card.getVTF(),
                        "One of the value in failure column is not accepted: " + card.getFailurePosition(),
                        expectedResult.getCrossTestResult(),
                        actualResult.getCrossTestResult()));
                // Out of the function once
                // failure is found
                return;
            } else {
                // No error found
                Logger.log(TAG, "No error found");
            }

        } else {

            if (ParserHelper.checkArrayContain(mExpectedNTList, card.getVTF())) {
                // check expected NT card
                Logger.log(TAG, card.getVTF() + " is NT Card");
                checkExpectedNTCard(card);
            } else if (ParserHelper.checkArrayContain(mNonID1List, card.getVTF())) {
                // check non ID1 card
                Logger.log(TAG, card.getVTF() + " is NON-ID1 Card");
                checkNonID1Card(card);
            } else {
                // check ID1 card
                Logger.log(TAG, card.getVTF() + " is ID1 Card");
                checkID1Card(card);
            }
        }
    }

    /**
     * Application write report contain the failure position in each
     * card and device combination.
     *
     * Application also give verdict of cross testing based on the threshold value
     * of 10% maximum. Cross testing is failed if there are more than 10 % of distance failures.
     */
    public void generate() {

        String reportName = Configuration.CONFIG_PATH + File.separator + Configuration.getInstance().getValue(Configuration.INPUT_FILE_NAME);
        String generatedReportName = Configuration.CONFIG_PATH + File.separator + Configuration.getInstance().getValue(Configuration.OUTPUT_FILE_NAME);
        String timestamp = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aaa").format(new Timestamp(System.currentTimeMillis()));

        // Create new excel file
        File file = new File(generatedReportName);
        if (file.exists()) {
            file.delete();
        }
        
        // Create excel document if no error exist
        if (!Logger.checkError(reportName, generatedReportName, timestamp)) {

            try {

                // Prepare writeable area
                WorkbookSettings wbSettings = new WorkbookSettings();
                wbSettings.setLocale(new Locale("en", "EN"));
                WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
                workbook.createSheet("Report", 0);
                WritableSheet excelSheet = workbook.getSheet(0);

                // Prepare title cell format
                WritableFont cellFontTitle = new WritableFont(WritableFont.ARIAL, 12);
                cellFontTitle.setBoldStyle(WritableFont.BOLD);
                WritableCellFormat cellFormatTitle = new WritableCellFormat(cellFontTitle);
                cellFormatTitle.setAlignment(Alignment.LEFT);
                cellFormatTitle.setVerticalAlignment(VerticalAlignment.TOP);
                cellFormatTitle.setBorder(Border.ALL, BorderLineStyle.THIN);

                // Prepare content cell format
                WritableFont cellFontContent = new WritableFont(WritableFont.ARIAL, 12);
                WritableCellFormat cellFormatContent = new WritableCellFormat(cellFontContent);
                cellFormatContent.setAlignment(Alignment.LEFT);
                cellFormatContent.setVerticalAlignment(VerticalAlignment.TOP);
                cellFormatContent.setBorder(Border.ALL, BorderLineStyle.THIN);

                // Writing operation
                int columnVTF = 1;
                int columnFailures = 2;
                int row = 1;
                int rowMergeStart = 0;
                int rowMergeEnd = 0;
                
                // Write Metadata
                excelSheet.addCell(new Label(columnVTF, row, "Report name:"));
                excelSheet.addCell(new Label(columnVTF + 1, row, Configuration.getInstance().getValue(Configuration.INPUT_FILE_NAME)));
                row++;
                excelSheet.addCell(new Label(columnVTF, row, "Time created:"));
                excelSheet.addCell(new Label(columnVTF + 1, row, timestamp));
                row++;
                
                // Write Title
                row++;
                excelSheet.addCell(new Label(columnVTF, row, "VTF", cellFormatTitle));
                excelSheet.addCell(new Label(columnFailures, row, "FAILURES", cellFormatTitle));
                row++;

                // Write report for Failed Cards
                for (Card card : mFailedCardPool) {                    
                    rowMergeStart = row;
                    Logger.warning("@@@@@@@@@@ " + card.getVTF() + ":");
                    // Write failures
                    for (String text : card.getAnalysedResult().getCrossTestReport()) {
                        excelSheet.addCell(new Label(columnFailures, row, text, cellFormatContent));
                        Logger.warning("@@@@@@@@@@ * " + text);
                        row++;
                    }
                    rowMergeEnd = row - 1;
                    // Write VTF
                    excelSheet.mergeCells(columnVTF, rowMergeStart, columnVTF, rowMergeEnd);
                    excelSheet.addCell(new Label(columnVTF, rowMergeStart, card.getVTF(), cellFormatContent));
                    Logger.warning("@@@@@@@@@@");
                }

                // Write report for NT Cards
                for (Card card : mNTCardPool) {    
                    // Write VTF
                    excelSheet.addCell(new Label(columnVTF, row, card.getVTF(), cellFormatContent));
                    Logger.warning("@@@@@@@@@@ " + card.getVTF() + ":");
                    // Write failures
                    String observations = "Not Tested due to " + card.getObservations();
                    excelSheet.addCell(new Label(columnFailures, row, observations, cellFormatContent));
                    Logger.warning("@@@@@@@@@@ * " + observations);
                    Logger.warning("@@@@@@@@@@");
                    row++;
                }

                // Adjust column length
                excelSheet.setColumnView(columnVTF, 16);
                excelSheet.setColumnView(columnFailures, 45);

                Logger.warning("@@@@@@@@@@ --------------------------------------------------------------------");
                
                // Write summary of XT
                int pool = mCardPool.size();
                int pass = mPassCardPool.size();
                int failed = mFailedCardPool.size();
                int NT = mNTCardPool.size();

                int totalTested = pass + failed;
                double passPercentage = ((double) pass / (double) totalTested) * 100;
                DecimalFormat df = new DecimalFormat("####0.00");
                
                row++;
                excelSheet.addCell(new Label(columnVTF, row, "Pass:"));
                excelSheet.addCell(new Label(columnVTF + 1, row, "" + pass));
                row++;
                excelSheet.addCell(new Label(columnVTF, row, "Fail:"));
                excelSheet.addCell(new Label(columnVTF + 1, row, "" + failed));
                row++;   
                excelSheet.addCell(new Label(columnVTF, row, "NT:"));
                excelSheet.addCell(new Label(columnVTF + 1, row, "" + NT));                       
                row++;
                excelSheet.addCell(new Label(columnVTF, row, "Total Tested:"));
                excelSheet.addCell(new Label(columnVTF + 1, row, "" + totalTested));
                row++;   
                excelSheet.addCell(new Label(columnVTF, row, "Pool Size:"));
                excelSheet.addCell(new Label(columnVTF + 1, row, "" + pool));
                row++;   
                excelSheet.addCell(new Label(columnVTF, row, "Pass Percentage:"));
                excelSheet.addCell(new Label(columnVTF + 1, row, "" + df.format(passPercentage) + "%"));
                row++;                

                Logger.warning("@@@@@@@@@@ Pass             : " + pass);
                Logger.warning("@@@@@@@@@@ Fail             : " + failed);
                Logger.warning("@@@@@@@@@@ NT               : " + NT);
                Logger.warning("@@@@@@@@@@ --------------------------------------------------------------------");
                Logger.warning("@@@@@@@@@@ Total Tested     : " + totalTested);
                Logger.warning("@@@@@@@@@@ Pool Size        : " + pool);
                Logger.warning("@@@@@@@@@@ Pass Percentage  : " + df.format(passPercentage) + "%");
                Logger.warning("@@@@@@@@@@ --------------------------------------------------------------------");

                // Ending operation
                workbook.write();
                workbook.close();

            } catch (WriteException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
